//
//  ViewController.swift
//  jtbizzle
//
//  Created by Izureru on 23/07/2015.
//  Copyright (c) 2015 Izureru. All rights reserved.
//

import UIKit
import MapKit

class ViewController: UIViewController {

  @IBOutlet weak var mapView: MKMapView!
  
  override func viewDidLoad() {
    super.viewDidLoad()
//    let initialLocation = CLLocation(latitude: 17.9833, longitude: -76.8000)
    let initialLocation = CLLocation(latitude: 21.282778, longitude: -157.829444)
    let regionRadius: CLLocationDistance = 1000
    func centerMapOnLocation(location: CLLocation) {
      let coordinateRegion = MKCoordinateRegionMakeWithDistance(location.coordinate,
        regionRadius * 2.0, regionRadius * 2.0)
      mapView.setRegion(coordinateRegion, animated: true)
    }
    centerMapOnLocation(initialLocation)
    mapView.delegate = self
    // show artwork on map
    let artwork = Artwork(title: "King David Kalakaua",
      locationName: "Waikiki Gateway Park",
      discipline: "Sculpture",
      coordinate: CLLocationCoordinate2D(latitude: 21.283921, longitude: -157.831661))
    
    mapView.addAnnotation(artwork)
  }
}

